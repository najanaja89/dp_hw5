﻿

using System;

namespace TemplateMethod
{

    abstract class DataMiner
    {

        public void Mine()
        {
            this.OpenFile();
            this.ParseData();
            this.ExtractData();
            this.AnalyzeData();
            this.SendReport();
            this.CloseFile();
            this.ArchiveFile();
        }

        protected void AnalyzeData()
        {
            Console.WriteLine("DataMiner says: I am analyzing data");
        }

        protected void SendReport()
        {
            Console.WriteLine("DataMiner says: I'm sending report");
        }

        protected abstract void ParseData();

        protected abstract void ExtractData();


        protected virtual void OpenFile()
        {
            Console.WriteLine("DataMiner says: I'm opening file");
        }

        protected virtual void CloseFile()
        {
            Console.WriteLine("DataMiner says: I'm closing file");
        }

        //Добавлянем функционал архивирования
        protected virtual void ArchiveFile()
        {
            Console.WriteLine("DataMiner says: I'm arhcive file");
        }
    }


    class XMLDataMiner : DataMiner
    {
        protected override void ParseData()
        {
            Console.WriteLine("XMLDataMiner says: I'm parsing data");
        }

        protected override void ExtractData()
        {
            Console.WriteLine("XMLDataMiner says: I'm extracting data");
        }
    }

    class JsonDataMiner : DataMiner
    {
        protected override void ParseData()
        {
            Console.WriteLine("JsonDataMiner says: I'm parsing data");
        }

        protected override void ExtractData()
        {
            Console.WriteLine("JsonDataMiner says: I'm extracting data");
        }
    }


    class PDFDataMiner : DataMiner
    {
        protected override void ParseData()
        {
            Console.WriteLine("PDFDataMiner says: I'm parsing data");
        }

        protected override void ExtractData()
        {
            Console.WriteLine("PDFDataMiner says: I'm extracting data");
        }

        protected override void OpenFile()
        {
            Console.WriteLine("PDFDataMiner says: Overridden Opening file");
        }
    }

    class Client
    {

        public static void ClientCode(DataMiner abstractClass)
        {
            abstractClass.Mine();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {

            //В базовый класс добавлена функция архиварования, реализовання черех Хуки
            Console.WriteLine("Same client code can work with different subclasses:");
            Client.ClientCode(new XMLDataMiner());
            Console.Write("\n");

            Console.WriteLine("Same client code can work with different subclasses:");
            Client.ClientCode(new PDFDataMiner());
            Console.Write("\n");

            Console.WriteLine("Same client code can work with different subclasses:");
            Client.ClientCode(new JsonDataMiner());
            Console.Write("\n");

            Console.ReadLine();
        }
    }
}
